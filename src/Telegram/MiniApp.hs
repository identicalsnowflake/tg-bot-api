module Telegram.MiniApp where

import Control.Monad
import qualified Data.Aeson as AE
import Data.Base16.Types
import qualified Data.ByteString as BS
import Data.ByteString.Base16
import qualified Data.ByteString.Lazy as LBS
import qualified Data.ByteArray as Mem
import Data.Functor
import Data.List
import Data.Text(Text)
import Data.Text.Encoding
import qualified Network.HTTP.Types.URI as URI
import qualified Crypto.MAC.HMAC as Crypto
import Crypto.Hash.Algorithms (SHA256)

import qualified Telegram.Types as T


-- | Verify the authenticity of the init data provided to a bot's mini app. If the authentication fails, returns @Nothing@; if authentication succeeds, return the user data
miniAppInitVerification :: Text -- | Bot token
                        -> BS.ByteString -- | Raw init data
                        -> Maybe T.User
miniAppInitVerification botTokenT rawInitData = do
  -- the underlying data in theory could contain more/less info than we need, depending on the
  -- circumstances, but for the foreseeable future, I don't think I care. but just in case:
  -- https://docs.telegram-mini-apps.com/platform/init-data
  let botToken :: BS.ByteString = encodeUtf8 botTokenT
  case extractHash (URI.parseQuery rawInitData) of
    Just (qs,h) -> do
      let sqs = sortOn fst qs
      let is = sqs <&> \(a,b) -> a <> "=" <> maybe "" id b
      let t = BS.intercalate "\n" is
      -- let !() = unsafePerformIO do
      --             BS.putStr t
      let secr :: BS.ByteString = Mem.convert
                                    $ Crypto.hmacGetDigest
                                    $ Crypto.hmac @_ @_ @SHA256 ("WebAppData" :: BS.ByteString) botToken
      let hash :: BS.ByteString = Mem.convert
                                    $ Crypto.hmacGetDigest
                                    $ Crypto.hmac @_ @_ @SHA256 secr t
      if extractBase16 (encodeBase16' hash) == h
         then case join (snd <$> find (\(k,_) -> k == "user") qs) >>= AE.decode @T.User . LBS.fromStrict of
                Nothing -> error "miniAppInitVerification: unexpected lack of user data in mini app init data"
                Just u -> Just u
         else Nothing
    Nothing -> Nothing
  where
    extractHash :: [ URI.QueryItem ] -> Maybe ([ URI.QueryItem ] , BS.ByteString)
    extractHash qs = case find (\(k,_) -> k == "hash") qs of
      Just (_ , Just h) -> Just (filter (\(k,_) -> k /= "hash") qs , h)
      _ -> Nothing

