{-# LANGUAGE RecordWildCards #-}
{-# LANGUAGE DuplicateRecordFields #-}
{-# LANGUAGE StrictData #-}

module Telegram.Types where

import Control.Monad
import Data.Aeson
import Data.Aeson.TH
import Data.Hashable (Hashable)
import Data.Text (Text)
import GHC.Int (Int64)
import GHC.Generics (Generic)
import Prelude hiding (id , length)


type LanguageCode = Text -- A two-letter ISO 639-1 language code


-- Telegram makes frequent use of integer types that "fit within double range"
newtype TInt = TInt { runTInt :: Int64 }
  deriving stock Generic
  deriving newtype (Eq,Ord,Show,Hashable)

instance ToJSON TInt where
  toJSON (TInt x) = toJSON (fromIntegral x :: Double)

instance FromJSON TInt where
  parseJSON = fmap (\(y :: Double) -> TInt (round y)) . parseJSON

-- unix timestamp in seconds
newtype Timestamp = Timestamp TInt
  deriving stock Generic
  deriving newtype (Eq,Ord,Show,ToJSON,FromJSON,Hashable)

newtype CallbackQueryId = CallbackQueryId Text
  deriving stock Generic
  deriving newtype (Eq,Ord,Show,ToJSON,FromJSON,Hashable)

newtype ChatId = ChatId TInt
  deriving stock Generic
  deriving newtype (Eq,Ord,Show,ToJSON,FromJSON,Hashable)

newtype FileId = FileId Text
  deriving stock Generic
  deriving newtype (Eq,Ord,Show,ToJSON,FromJSON,Hashable)

newtype FoursquareId = FoursquareId Text
  deriving stock Generic
  deriving newtype (Eq,Ord,Show,ToJSON,FromJSON,Hashable)

newtype InlineQueryId = InlineQueryId Text
  deriving stock Generic
  deriving newtype (Eq,Ord,Show,ToJSON,FromJSON,Hashable)

newtype MessageId = MessageId { runMessageId :: TInt }
  deriving stock Generic
  deriving newtype (Eq,Ord,Show,ToJSON,FromJSON,Hashable)

newtype PollId = PollId Text
  deriving stock Generic
  deriving newtype (Eq,Ord,Show,ToJSON,FromJSON,Hashable)

newtype PreCheckoutQueryId = PreCheckoutQueryId Text
  deriving stock Generic
  deriving newtype (Eq,Ord,Show,ToJSON,FromJSON,Hashable)

newtype ProviderPaymentId = ProviderPaymentId Text
  deriving stock Generic
  deriving newtype (Eq,Ord,Show,ToJSON,FromJSON,Hashable)

newtype ResultId = ResultId Text
  deriving stock Generic
  deriving newtype (Eq,Ord,Show,ToJSON,FromJSON,Hashable)

newtype ShippingOptionId = ShippingOptionId Text
  deriving stock Generic
  deriving newtype (Eq,Ord,Show,ToJSON,FromJSON,Hashable)

newtype ShippingQueryId = ShippingQueryId Text
  deriving stock Generic
  deriving newtype (Eq,Ord,Show,ToJSON,FromJSON,Hashable)

newtype TelegramPaymentId = TelegramPaymentId Text
  deriving stock Generic
  deriving newtype (Eq,Ord,Show,ToJSON,FromJSON,Hashable)

newtype UpdateId = UpdateId { runUpdateId :: TInt }
  deriving stock Generic
  deriving newtype (Eq,Ord,Show,ToJSON,FromJSON,Hashable)

newtype UserId = UserId TInt
  deriving stock Generic
  deriving newtype (Eq,Ord,Show,ToJSON,FromJSON,Hashable)


-- GFM (Github-flavored Markdown) isn't actually supported by Telegram directly; rather, if
-- chosen, this library will parse it with cmark-gfm and render that out to Telegram's MarkdownV2.
--
-- The reason for this is Telegram's markdown parsers are extremely brittle, and break in all sorts of ways that seem stupid to me, whereas the GFM approach mostly works as I expect and want.
data ParseMode = GFM | Markdown | MarkdownV2 | HTML deriving (Generic,Eq,Ord,Show,ToJSON)

data User = User {
    id :: UserId
  , is_bot :: Maybe Bool
  , first_name :: Text
  , last_name :: Maybe Text
  , username :: Maybe Text
  -- , language_code :: Maybe Text -- IETF language code
  } deriving (Generic,Eq,Ord,Show,ToJSON,FromJSON)


data ChatType =
    Direct
  | NonDirect GroupType
  deriving (Generic,Eq,Ord,Show)

data GroupType =
    Group
  | Supergroup
  | Channel
  deriving (Generic,Eq,Ord,Show)

data PhotoSize = PhotoSize {
    file_id :: FileId
  , width :: Integer
  , height :: Integer
  , file_size :: Maybe Integer
  } deriving (Generic,Eq,Ord,Show,ToJSON,FromJSON)

data MessageEntity =
    Mention (Either Text User) -- username OR User if no username exists
  | HashTag Text
  | CashTag Text
  | BotCommand Text
  | Email Text
  | Phone Text
  | Bold Text
  | Italic Text
  | MonowidthString Text
  | MonowidthBlock Text
  | URL { text :: Text , link :: Text }
  deriving (Generic,Eq,Ord,Show,ToJSON,FromJSON)


data PollOption = PollOption {
    text :: Text
  , voter_count :: Integer
  } deriving (Generic,Eq,Ord,Show,ToJSON,FromJSON)


data MaskPosition = MaskPosition {
    point :: Text
  , x_shift :: Double
  , y_shift :: Double
  , scale :: Double
  } deriving (Generic,Eq,Ord,Show,ToJSON,FromJSON)

data Chat = Chat {
    chat_id :: ChatId
  , chat_type :: ChatType
  } deriving (Generic,Eq,Ord,Show)


data ShippingAddress = ShippingAddress {
    country_code :: Text -- 2-letter ISO 3166-1
  , state :: Maybe Text
  , city :: Text
  , street_line1 :: Text
  , street_line2 :: Text
  , post_code :: Text
  } deriving (Generic,Eq,Ord,Show,ToJSON,FromJSON)

data OrderInfo = OrderInfo {
    name :: Maybe Text
  , phone_number :: Maybe Text
  , email :: Maybe Text
  , shipping_address :: Maybe ShippingAddress
  } deriving (Generic,Eq,Ord,Show,ToJSON,FromJSON)

data Animation = Animation {
    file_id :: FileId
  , width :: Integer
  , height :: Integer
  , duration :: Integer -- in seconds
  , thumbnail :: Maybe PhotoSize
  , file_name :: Maybe Text
  , mime_type :: Maybe Text
  , file_size :: Maybe Integer
  } deriving (Generic,Eq,Ord,Show,ToJSON,FromJSON)

data Location = Location {
    latitude :: Double
  , longitude :: Double
  } deriving (Generic,Eq,Ord,Show,ToJSON,FromJSON)

data Media =
    Animated Animation
  | Audio {
      file_id :: FileId
    , duration :: Integer -- in seconds
    , performer :: Maybe Text
    , title :: Maybe Text
    , mime_type :: Maybe Text
    , file_size :: Maybe Integer
    , thumbnail :: Maybe PhotoSize
    }
  | Document {
      file_id :: FileId
    , thumbnail :: Maybe PhotoSize
    , file_name :: Maybe Text
    , mime_type :: Maybe Text
    , file_size :: Maybe Integer
    }
  | Photo [ PhotoSize ]
  | Video {
      file_id :: FileId
    , width :: Integer
    , height :: Integer
    , duration :: Integer
    , thumbnail :: Maybe PhotoSize
    , mime_type :: Maybe Text
    , file_size :: Maybe Integer
    }
  | VideoNote {
      file_id :: FileId
    , content_size :: Integer -- video with and height (o____O)
    , duration :: Integer
    , thumbnail :: Maybe PhotoSize
    , file_size :: Maybe Integer
    }
  | Voice {
      file_id :: FileId
    , duration :: Integer
    , mime_type :: Maybe Text
    , file_size :: Maybe Integer
    }
  deriving (Generic,Eq,Ord,Show)

data MessageContent =
    Contact {
      phone_number :: Text
    , first_name :: Text
    , last_name :: Maybe Text
    , user_id :: Maybe UserId
    , vcard :: Maybe Text
    }
  | Game {
      title :: Text
    , description :: Text
    , photo :: [ PhotoSize ]
    , text :: Maybe Text
    , text_entities :: Maybe [ MessageEntity ]
    , animation :: Maybe Animation
    }
  | LocationInfo Location
  | Media {
      caption :: Maybe (Text , [ MessageEntity ])
    , media :: Media
    }
  | Poll {
      poll_id :: PollId
    , question :: Text
    , options :: [ PollOption ]
    , is_closed :: Bool
    }
  | Sticker {
      file_id :: FileId
    , width :: Integer
    , height :: Integer
    , thumbnail :: Maybe PhotoSize
    , emoji :: Maybe Text
    , set_name :: Maybe Text
    , mask_position :: Maybe MaskPosition
    , file_size :: Maybe Integer
    }
  | Textual {
      message :: Text
    , entities :: [ MessageEntity ]
    }
  | Venue {
      location :: Location
    , title :: Text
    , address :: Text
    , foursquare_id :: Maybe FoursquareId
    , foursquare_type :: Maybe Text
    }
  deriving (Generic,Eq,Ord,Show)

data Message = Message {
      message_id :: MessageId
    , forward_from :: Maybe User
    , forward_from_chat :: Maybe Chat
    , forward_from_message_id :: Maybe MessageId
    , forward_date :: Maybe Timestamp
    , reply_to_message :: Maybe MessageId
    , edit_date :: Maybe Timestamp
    , content :: MessageContent
    }
  deriving (Generic,Eq,Ord,Show)

data MessageNotification =
    ConnectedWebsite Text
  | IncomingMessage Message
  | Invoice {
      title :: Text
    , description :: Text
    , start_parameter :: Text -- "Unique bot deep-linking parameter that can be used to generate this invoice"
    , currency :: Text -- Three-letter ISO 4217 currency code
    , total_amount :: Integer -- total amount in smallest unit of currency
    }
  | SuccessfulPayment {
      currency :: Text -- Three-letter ISO 4217 currrency code
    , total_amount :: Integer -- in smallest unit of currency
    , invoice_payload :: Text -- bot-specified invoice payload
    , shipping_option_id :: Maybe ShippingOptionId -- choice of shipping
    , order_info :: Maybe OrderInfo
    , telegram_payment_charge_id :: TelegramPaymentId
    , provider_payment_charge_id :: ProviderPaymentId
    }
  deriving (Generic,Eq,Ord,Show)

data GroupNotification =
    ExitMember User
  | General MessageNotification
  | GroupPhotoChange [ PhotoSize ]
  | GroupTitleChange Text
  | NewMembers [ User ]
  | Pinned MessageId
  deriving (Generic,Eq,Ord,Show)

data Notification =
    DM MessageNotification
  | GroupNotification GroupType GroupNotification
  deriving (Generic,Eq,Ord,Show)

data CallbackContent =
    Data Text
  | GameShortName Text
  deriving (Generic,Eq,Ord,Show)

data ChatInviteLink = ChatInviteLink {
    invite_link :: Text
  , creator :: User
  , creates_join_request :: Bool
  , is_primary :: Bool
  , is_revoked :: Bool
  , name :: Maybe Text
  , expire_date :: Maybe Int -- unix timestamp
  , member_limit :: Maybe Int
  , pending_join_request_count :: Maybe Int
  } deriving (Generic,Eq,Ord,Show)

data Incoming =
    New {
      chat_id :: ChatId
    , message_id :: MessageId
    , from :: Maybe User
    , date :: Timestamp
    , notification :: Notification
    }
  | Edit {
      chat_id :: ChatId
    , message_id :: MessageId
    , from :: Maybe User
    , date :: Timestamp
    , notification :: Notification
    }
  | InlineQuery {
      qid :: InlineQueryId
    , sender :: User
    , location :: Maybe Location
    , query :: Text
    , offset :: Text
    }
  | ChosenInlineResult {
      result_id :: ResultId
    , sender :: User
    , location :: Maybe Location
    , inline_message_id :: Maybe MessageId
    , query :: Text
    }
  | CallbackQuery {
      callback_query_id :: CallbackQueryId
    , sender :: User
    , chat :: Chat
    , messageId :: MessageId
    , content :: CallbackContent
    }
  | ShippingQuery {
      shipping_id :: ShippingQueryId
    , sender :: User
    , invoice_payload :: Text
    , shipping_address :: ShippingAddress
    }
  | PreCheckoutQuery {
      cqid :: PreCheckoutQueryId
    , sender :: User
    , currency :: Text -- Three-letter ISO 4217 currency code
    , total_amount :: Integer -- in smallest units of currency
    , invoice_payload :: Text
    , shipping_option_id :: Maybe ShippingOptionId
    , order_info :: Maybe OrderInfo
    }
  | ChatJoinRequest {
      chat :: Chat
    , user :: User
    , user_chat_id :: ChatId
    , date :: Timestamp
    , bio :: Maybe Text
    , invite_link :: ChatInviteLink
    }
  -- -| PollStatus Poll -- removing this since literally every other type has a user
                       -- in context, making it quite awkward that this one
                       -- corner case does not.
  deriving (Generic,Eq,Ord,Show)

data InlineQueryResult = InlineQueryResultCachedVideo { -- TODO: others
    id :: Text
  , video_file_id :: FileId
  , title :: Text
  , description :: Maybe Text
  , caption :: Maybe Text
  , parse_mode :: Maybe ParseMode
  , show_caption_above_media :: Maybe Bool
  -- , caption_entities :: Maybe [ MessageEntity ]
  -- , reply_markup :: InlineKeyboardMarkup
  -- , input_message_content :: InputMessageContent
  }
  | InlineQueryResultCachedPhoto {
        id :: Text
      , photo_file_id :: FileId
      }

data InlineKeyboardButton = InlineKeyboardButton {
    text :: Text
  , url :: Maybe Text
  , callback_data :: Maybe Text
  -- web_app
  -- login_url
  -- switch_inline_query
  -- switch_inline_query_current_chat
  -- switch_inline_query_chosen_chat
  -- callback_game
  -- pay
  } deriving (Generic,Eq,Ord,Show)

$(deriveJSON defaultOptions{ omitNothingFields = True } ''InlineKeyboardButton)

data InlineKeyboardMarkup = InlineKeyboardMarkup {
    inline_keyboard :: [ [ InlineKeyboardButton ] ]
  } deriving (Generic,Eq,Ord,Show,ToJSON,FromJSON)


data InlineQueryAnswer = InlineQueryAnswer {
    inline_query_id :: InlineQueryId
  , results :: [ InlineQueryResult ]
  , cache_time :: Maybe Int -- in seconds, default 300
  , is_personal :: Maybe Bool -- no global cache if yes
  , next_offset :: Maybe Text
  -- button :: ???
  } deriving Generic

data CallbackQueryAnswer = CallbackQueryAnswer {
    callback_query_id :: CallbackQueryId
  , text :: Maybe Text
  , show_alert :: Bool
  , url :: Maybe Text
  , cache_time :: Maybe Int -- in seconds, default 300
  -- button :: ???
  } deriving Generic


data EditMessageReplyMarkup = EditMessageReplyMarkup {
    chat_id :: ChatId
  , message_id :: MessageId
  , reply_markup :: Maybe InlineKeyboardMarkup
  } deriving Generic

data File = File {
    file_id :: FileId
  -- , file_unique_id :: UFileId
  , file_size :: Maybe Int
  , url :: Maybe Text
  } deriving (Generic,Eq,Ord,Show,FromJSON)

data CommandScope =
    CommandScopeDefault
  | CommandScopeAllPrivateChats
  | CommandScopeAllGroupChats
  | CommandScopeAllChatAdministrators
  | CommandScopeChat ChatId
  | CommandScopeChatAdministrators ChatId
  | CommandScopeChatMember ChatId UserId
  deriving (Generic,Eq,Ord,Show)

data Command = Command {
    command :: Text -- max 32 chars, lowercase alphanumeric and underscore
  , description :: Text -- max 256 chars
  } deriving (Generic,Eq,Ord,Show,ToJSON,FromJSON)

