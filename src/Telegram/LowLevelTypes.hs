{-# LANGUAGE RecordWildCards #-}
{-# LANGUAGE DuplicateRecordFields #-}
{-# LANGUAGE StrictData #-}

{-# OPTIONS_GHC -fno-warn-orphans #-}

module Telegram.LowLevelTypes where

import Control.Applicative
import Control.Lens hiding (from , (.=))
import Control.Monad
import Data.Aeson
import Data.Aeson.TH
import Data.Char (toLower)
import Data.Generics.Product
import Data.Maybe (fromJust)
import Data.Text (Text)
import qualified Data.Text as Text
import GHC.Generics (Generic)
import Prelude hiding (id , length)

import qualified Telegram.Types as T

data RawChatType =
    Private
  | Group
  | Supergroup
  | Channel
  deriving (Generic,Eq,Ord,Show)

$(deriveJSON defaultOptions{ constructorTagModifier = map toLower} ''RawChatType)


data RChat = RChat {
    _id :: T.ChatId
  , _type :: RawChatType
    -- lots of other stuff which is only sometimes here depending on context
  } deriving (Generic,Eq,Ord,Show)

$(deriveJSON defaultOptions{fieldLabelModifier = drop 1 } ''RChat)

instance ToJSON T.Chat where
  toJSON (T.Chat a T.Direct) = toJSON $ RChat a Private
  toJSON (T.Chat a (T.NonDirect T.Group)) = toJSON $ RChat a Group
  toJSON (T.Chat a (T.NonDirect T.Supergroup)) = toJSON $ RChat a Supergroup
  toJSON (T.Chat a (T.NonDirect T.Channel)) = toJSON $ RChat a Channel

instance FromJSON T.Chat where
  parseJSON x = (\(RChat a b) -> T.Chat a (f b)) <$> parseJSON x
    where
      f :: RawChatType -> T.ChatType
      f Private = T.Direct
      f Group = T.NonDirect T.Group
      f Supergroup = T.NonDirect T.Supergroup
      f Channel = T.NonDirect T.Channel



data RawMessageEntity = RawMessageEntity {
    _type :: Text
  , _offset :: T.TInt
  , _length :: T.TInt
  , _url :: Maybe Text
  , _user :: Maybe T.User
  } deriving (Generic,Eq,Ord,Show)

$(deriveJSON defaultOptions{fieldLabelModifier = drop 1 } ''RawMessageEntity)

-- Type of the entity. Can be mention (@username), hashtag, cashtag, bot_command, url, email, phone_number, bold (bold text), italic (italic text), code (monowidth string), pre (monowidth block), text_link (for clickable text URLs), text_mention (for users without usernames)

data Audio = Audio {
    file_id :: T.FileId
  , duration :: Integer -- in seconds
  , performer :: Maybe Text
  , title :: Maybe Text
  , mime_type :: Maybe Text
  , file_size :: Maybe Integer
  , thumbnail :: Maybe T.PhotoSize
  } deriving (Generic,Eq,Ord,Show,ToJSON,FromJSON)

-- "general file", seems to not imply actually being a document
data Document = Document {
    file_id :: T.FileId
  , thumbnail :: Maybe T.PhotoSize
  , file_name :: Maybe Text
  , mime_type :: Maybe Text
  , file_size :: Maybe Integer
  } deriving (Generic,Eq,Ord,Show,ToJSON,FromJSON)

data Game = Game {
    title :: Text
  , description :: Text
  , photo :: [ T.PhotoSize ]
  , text :: Maybe Text
  , text_entities :: Maybe [ RawMessageEntity ]
  , animation :: Maybe T.Animation
  } deriving (Generic,Eq,Ord,Show,ToJSON,FromJSON)


data Sticker = Sticker {
    file_id :: T.FileId
  , width :: Integer
  , height :: Integer
  , thumbnail :: Maybe T.PhotoSize
  , emoji :: Maybe Text
  , set_name :: Maybe Text
  , mask_position :: Maybe T.MaskPosition
  , file_size :: Maybe Integer
  } deriving (Generic,Eq,Ord,Show,ToJSON,FromJSON)

data Video = Video {
    file_id :: T.FileId
  , width :: Integer
  , height :: Integer
  , duration :: Integer
  , thumbnail :: Maybe T.PhotoSize
  , mime_type :: Maybe Text
  , file_size :: Maybe Integer
  } deriving (Generic,Eq,Ord,Show,ToJSON,FromJSON)

data Voice = Voice {
    file_id :: T.FileId
  , duration :: Integer
  , mime_type :: Maybe Text
  , file_size :: Maybe Integer
  } deriving (Generic,Eq,Ord,Show,ToJSON,FromJSON)

data VideoNote = VideoNote {
    file_id :: T.FileId
  , length :: Integer -- video with and height (o____O)
  , duration :: Integer
  , thumbnail :: Maybe T.PhotoSize
  , file_size :: Maybe Integer
  } deriving (Generic,Eq,Ord,Show,ToJSON,FromJSON)

data Contact = Contact {
    phone_number :: Text
  , first_name :: Text
  , last_name :: Maybe Text
  , user_id :: Maybe T.UserId
  , vcard :: Maybe Text
  } deriving (Generic,Eq,Ord,Show,ToJSON,FromJSON)

data Venue = Venue {
    location :: T.Location
  , title :: Text
  , address :: Text
  , foursquare_id :: Maybe T.FoursquareId
  , foursquare_type :: Maybe Text
  } deriving (Generic,Eq,Ord,Show,ToJSON,FromJSON)

data Poll = Poll {
    id :: T.PollId
  , question :: Text
  , options :: [ T.PollOption ]
  , is_closed :: Bool
  } deriving (Generic,Eq,Ord,Show,ToJSON,FromJSON)

data Invoice = Invoice {
    title :: Text
  , description :: Text
  , start_parameter :: Text -- "Unique bot deep-linking parameter that can be used to generate this invoice"
  , currency :: Text -- Three-letter ISO 4217 currency code
  , total_amount :: Integer -- total amount in smallest unit of currency
  } deriving (Generic,Eq,Ord,Show,ToJSON,FromJSON)

data SuccessfulPayment = SuccessfulPayment {
    currency :: Text -- Three-letter ISO 4217 currrency code
  , total_amount :: Integer -- in smallest unit of currency
  , invoice_payload :: Text -- bot-specified invoice payload
  , shipping_option_id :: Maybe T.ShippingOptionId -- choice of shipping
  , order_info :: Maybe T.OrderInfo
  , telegram_payment_charge_id :: T.TelegramPaymentId
  , provider_payment_charge_id :: T.ProviderPaymentId
  } deriving (Generic,Eq,Ord,Show,ToJSON,FromJSON)

data RawMessage = RawMessage {
    message_id :: T.MessageId
  , from :: Maybe T.User -- always present for private chat
  , date :: T.Timestamp
  , chat :: T.Chat
  , forward_from :: Maybe T.User
  , forward_from_chat :: Maybe T.Chat
  , forward_from_message_id :: Maybe T.MessageId
  , forward_date :: Maybe T.Timestamp
  , reply_to_message :: Maybe RawMessage
  , edit_date :: Maybe T.Timestamp
  -- , _media_group_id -- what is this?
  -- , _author_signature :: Maybe String -- what is this?
  , text :: Maybe Text
  , entities :: Maybe [ RawMessageEntity ]
  , caption_entities :: Maybe [ RawMessageEntity ]
  -- , many media types ...
  , audio :: Maybe Audio
  , document :: Maybe Document
  , animation :: Maybe T.Animation
  , game :: Maybe Game
  , photo :: Maybe [ T.PhotoSize ]
  , sticker :: Maybe Sticker
  , video :: Maybe Video
  , voice :: Maybe Voice
  , video_note :: Maybe VideoNote
  , contact :: Maybe Contact
  , location :: Maybe T.Location
  , venue :: Maybe Venue
  , poll :: Maybe Poll
  
  , new_chat_members :: Maybe [ T.User ]
  , left_chat_member :: Maybe T.User
  , new_chat_title :: Maybe Text
  , new_chat_photo :: Maybe [ T.PhotoSize ]
  -- more weird stuff like passport, chat migrations, etc
  , pinned_message :: Maybe RawMessage
  , invoice :: Maybe Invoice
  , successful_payment :: Maybe SuccessfulPayment
  , connected_website :: Maybe Text
  , caption :: Maybe Text
  } deriving (Generic,Eq,Ord,Show,ToJSON,FromJSON)

data InlineQuery = InlineQuery {
    id :: T.InlineQueryId
  , from :: T.User
  , location :: Maybe T.Location
  , query :: Text
  , offset :: Text
  } deriving (Generic,Eq,Ord,Show,ToJSON,FromJSON)


data ChosenInlineResult = ChosenInlineResult {
    result_id :: T.ResultId
  , from :: T.User
  , location :: Maybe T.Location
  , inline_message_id :: Maybe T.MessageId
  , query :: Text
  } deriving (Generic,Eq,Ord,Show,ToJSON,FromJSON)

data RawMessageRef = RawMessageRef {
    message_id :: T.MessageId
  , chat :: T.Chat
  } deriving (Generic,Eq,Ord,Show,ToJSON,FromJSON)

data CallbackQuery = CallbackQuery {
    _id :: T.CallbackQueryId
  , _from :: T.User
  , _message :: Maybe RawMessageRef
  , _inline_message_id :: Maybe T.MessageId -- i think?
  , _chat_instance :: Text
  , _data :: Maybe Text
  , _game_short_name :: Maybe Text
  } deriving (Generic,Eq,Ord,Show)

$(deriveJSON defaultOptions{fieldLabelModifier = drop 1 } ''CallbackQuery)


data ShippingQuery = ShippingQuery {
    id :: T.ShippingQueryId
  , from :: T.User
  , invoice_payload :: Text
  , shipping_address :: T.ShippingAddress
  } deriving (Generic,Eq,Ord,Show,ToJSON,FromJSON)


data PreCheckoutQuery = PreCheckoutQuery {
    id :: T.PreCheckoutQueryId
  , from :: T.User
  , currency :: Text -- Three-letter ISO 4217 currency code
  , total_amount :: Integer -- in smallest units of currency
  , invoice_payload :: Text
  , shipping_option_id :: Maybe T.ShippingOptionId
  , order_info :: Maybe T.OrderInfo
  } deriving (Generic,Eq,Ord,Show,ToJSON,FromJSON)

data RawChatInviteLink = RawChatInviteLink {
    invite_link :: Text
  , creator :: T.User
  , creates_join_request :: Bool
  , is_primary :: Bool
  , is_revoked :: Bool
  , name :: Maybe Text
  , expire_date :: Maybe T.TInt -- unix timestamp
  , member_limit :: Maybe T.TInt
  , pending_join_request_count :: Maybe T.TInt
  } deriving (Generic,Eq,Ord,Show,FromJSON)

instance FromJSON T.ChatInviteLink where
  parseJSON = parseJSON @RawChatInviteLink <&> fmap \RawChatInviteLink { .. } ->
    T.ChatInviteLink {
        expire_date = (\x -> fromIntegral x.runTInt) <$> expire_date
      , member_limit = (\x -> fromIntegral x.runTInt) <$> member_limit
      , pending_join_request_count = (\x -> fromIntegral x.runTInt) <$> pending_join_request_count
      , ..
      }

data ChatJoinRequest = ChatJoinRequest {
    chat :: T.Chat
  , from :: T.User
  , user_chat_id :: T.ChatId
  , date :: T.Timestamp
  , bio :: Maybe Text
  , invite_link :: T.ChatInviteLink
  } deriving (Generic,Eq,Ord,Show,FromJSON)

data RawUpdate = RawUpdate {
    update_id :: T.UpdateId
  , message :: Maybe RawMessage
  , edited_message :: Maybe RawMessage
  , channel_post :: Maybe RawMessage
  , edited_channel_post :: Maybe RawMessage
  , inline_query :: Maybe InlineQuery
  , chosen_inline_result :: Maybe ChosenInlineResult
  , callback_query :: Maybe CallbackQuery
  , shipping_query :: Maybe ShippingQuery
  , pre_checkout_query :: Maybe PreCheckoutQuery
  , chat_join_request :: Maybe ChatJoinRequest
  , poll :: Maybe Poll
  } deriving (Generic,Eq,Ord,Show,FromJSON)

data APIResponse a = APIResponse {
    ok :: Bool
  , result :: a
  } deriving (Generic,Eq,Ord,Show,FromJSON)


parseIncoming :: RawUpdate -> Maybe T.Incoming
parseIncoming r =
      new
  <|> edit
  <|> cnew
  <|> cedit
  <|> inlineQuery
  <|> chosenInlineResult
  <|> callbackQuery
  <|> shippingQuery
  <|> precheckoutQuery
  <|> chatJoinRequest
  -- <|> polls
  where
    new :: Maybe T.Incoming
    new = join $ view (field @"message") r <&> parseIncoming' T.New

    edit :: Maybe T.Incoming
    edit = join $ view (field @"edited_message") r <&> parseIncoming' T.Edit

    cnew :: Maybe T.Incoming
    cnew = join $ view (field @"channel_post") r <&> parseIncoming' T.New

    cedit :: Maybe T.Incoming
    cedit = join $ view (field @"edited_channel_post") r <&> parseIncoming' T.Edit

    inlineQuery :: Maybe T.Incoming
    inlineQuery = r.inline_query <&> \InlineQuery { .. } ->
      T.InlineQuery { qid = id , sender = from , .. }

    chosenInlineResult :: Maybe T.Incoming
    chosenInlineResult = r.chosen_inline_result <&> \ChosenInlineResult { .. } ->
      T.ChosenInlineResult { sender = from , .. }

    callbackQuery :: Maybe T.Incoming
    callbackQuery = join $ r.callback_query <&> \CallbackQuery { .. } ->
      let content = case _data of
            Just d -> T.Data d
            Nothing -> T.GameShortName $ fromJust _game_short_name
      in
      case (_message , _inline_message_id) of
        (Nothing , Just _) -> error "tg_bot_api: TODO inline_message_id CallbackQuery" -- Just $
          -- T.CallbackQuery {
          --     cid = _id
          --   , sender = _from
          --   , chat_instance = _chat_instance
          --   , inline = True
          --   , messageId = x
          --   , ..
          --   }
        (Just x , _) -> Just $
          T.CallbackQuery {
              callback_query_id = _id
            , sender = _from
            , chat = view (field @"chat") x
            , messageId = view (field @"message_id") x
            , ..
            }
        _ -> Nothing

    shippingQuery :: Maybe T.Incoming
    shippingQuery = r.shipping_query <&> \ShippingQuery { .. } ->
      T.ShippingQuery { shipping_id = id , sender = from , .. }

    precheckoutQuery :: Maybe T.Incoming
    precheckoutQuery = r.pre_checkout_query <&> \PreCheckoutQuery { .. } ->
      T.PreCheckoutQuery { cqid = id , sender = from , .. }

    chatJoinRequest :: Maybe T.Incoming
    chatJoinRequest = r.chat_join_request <&> \ChatJoinRequest { .. } -> T.ChatJoinRequest {
        user = from
      , ..
      }
    -- polls :: Maybe Incoming
    -- polls = view (field @"poll") r <&> PollStatus
      

parseIncoming' :: (T.ChatId -> T.MessageId -> Maybe T.User -> T.Timestamp -> T.Notification -> T.Incoming) -> RawMessage -> Maybe T.Incoming
parseIncoming' c r = fmap (c (view (field @"chat" . field @"chat_id") r) (view (field @"message_id") r) (view (field @"from") r) (view (field @"date") r)) $
      exitMember
  <|> groupPhotoChange
  <|> groupTitleChange
  <|> newMembers
  <|> pinned
  <|> general
  where
    groupType :: Maybe T.GroupType
    groupType = case view (field @"chat" . field @"chat_type") r of
      T.Direct -> Nothing
      T.NonDirect x -> Just x
    
    exitMember :: Maybe T.Notification
    exitMember = T.GroupNotification <$> groupType <*> do
      T.ExitMember <$> r.left_chat_member

    groupPhotoChange :: Maybe T.Notification
    groupPhotoChange = T.GroupNotification <$> groupType <*> do
      T.GroupPhotoChange <$> r.new_chat_photo

    groupTitleChange :: Maybe T.Notification
    groupTitleChange = T.GroupNotification <$> groupType <*> do
      T.GroupTitleChange <$> r.new_chat_title

    newMembers :: Maybe T.Notification
    newMembers = T.GroupNotification <$> groupType <*> do
      T.NewMembers <$> r.new_chat_members

    pinned :: Maybe T.Notification
    pinned = T.GroupNotification <$> groupType <*> do
      T.Pinned . view (field @"message_id") <$> r.pinned_message

    general :: Maybe T.Notification
    general = case groupType of
      Nothing -> T.DM <$> notification
      Just w -> T.GroupNotification w . T.General <$> notification

    notification :: Maybe T.MessageNotification
    notification =
          connectedWebsite
      <|> invoice
      <|> successfulPayment
      <|> message'
      where
        connectedWebsite :: Maybe T.MessageNotification
        connectedWebsite = T.ConnectedWebsite <$> r.connected_website

        invoice :: Maybe T.MessageNotification
        invoice = view (field @"invoice") r <&> \Invoice { .. } -> T.Invoice { .. }

        successfulPayment :: Maybe T.MessageNotification
        successfulPayment = r.successful_payment
          <&> \SuccessfulPayment { .. } -> T.SuccessfulPayment { .. }

        message' :: Maybe T.MessageNotification
        message' = T.IncomingMessage <$> parseRawMessage r
            

-- must be the kind of message you get back from APIs like `sendMessage` or `sendPhoto`, not
-- the kind that are received as updates for group notifications like group title change,
-- as these are parsed differently
parseRawMessage :: RawMessage -> Maybe T.Message
parseRawMessage r = case r of
  RawMessage { .. } -> flip fmap c' \c -> T.Message {
      content = c
    , reply_to_message = view (field @"message_id")
        <$> view (field @"reply_to_message") r
    , ..
    }
  where
    c' =     contact
         <|> game
         <|> location'
         <|> poll
         <|> sticker
         <|> venue
         <|> media
         <|> textual
         where
            contact :: Maybe T.MessageContent
            contact = view (field @"contact") r <&> \Contact { .. } -> T.Contact { .. }

            game :: Maybe T.MessageContent
            game = view (field @"game") r <&> \Game { .. } ->
                T.Game {
                    text_entities = parseRawEntity <$> text >>= \f -> fmap f <$> text_entities
                  , ..
                  }

            location' :: Maybe T.MessageContent
            location' = T.LocationInfo <$> view (field @"location") r

            poll :: Maybe T.MessageContent
            poll = view (field @"poll") r <&> \Poll { .. } -> T.Poll { poll_id = id , .. }

            sticker :: Maybe T.MessageContent
            sticker = view (field @"sticker") r <&> \Sticker { .. } -> T.Sticker { .. }

            venue :: Maybe T.MessageContent
            venue = view (field @"venue") r <&> \Venue { .. } -> T.Venue { .. }

            media :: Maybe T.MessageContent
            media = fmap (T.Media captions) $
                  animation
              <|> audio
              <|> photo
              <|> video
              <|> videoNote
              <|> voice
              <|> document
              where
                animation :: Maybe T.Media
                animation = T.Animated <$> view (field @"animation") r

                audio :: Maybe T.Media
                audio = view (field @"audio") r <&> \Audio { .. } -> T.Audio { .. }

                document :: Maybe T.Media
                document = view (field @"document") r <&> \Document { .. } -> T.Document { .. }

                photo :: Maybe T.Media
                photo = T.Photo <$> view (field @"photo") r

                video :: Maybe T.Media
                video = view (field @"video") r <&> \Video { .. } -> T.Video { .. }

                videoNote :: Maybe T.Media
                videoNote = view (field @"video_note") r <&> \VideoNote { .. } -> T.VideoNote {
                  content_size = length , .. }

                voice :: Maybe T.Media
                voice = view (field @"voice") r <&> \Voice { .. } -> T.Voice { .. }
                
                captions :: Maybe (Text , [ T.MessageEntity ])
                captions = case (view (field @"caption") r , view (field @"caption_entities") r) of
                  (Just t , Just xs) -> Just (t , parseRawEntity t <$> xs)
                  (Just t , _) -> Just (t , [])
                  _ -> Nothing

            textual :: Maybe T.MessageContent
            textual = view (field @"text") r <&> \t -> T.Textual t $
              maybe [] (fmap $ parseRawEntity t) $ view (field @"entities") r

parseRawEntity :: Text -> RawMessageEntity -> T.MessageEntity
parseRawEntity t r@(RawMessageEntity { _type = y }) = case y of
  "mention" -> T.Mention $ Left $ Text.drop 1 s
  "hashtag" -> T.HashTag $ Text.drop 1 s
  "cashtag" -> T.CashTag s
  "bot_command" -> T.BotCommand $ Text.drop 1 s
  "url" -> T.URL s s
  "email" -> T.Email s
  "phone_number" -> T.Phone s
  "bold" -> T.Bold s
  "italic" -> T.Italic s
  "code" -> T.MonowidthString s
  "pre" -> T.MonowidthBlock s
  "text_link" -> T.URL s (fromJust r._url)
  "text_mention" -> T.Mention $ Right (fromJust r._user)
  v -> error $ "parse fail - unknown entity type " <> show v
  where
    s = (Text.take . fromIntegral $ r._length.runTInt)
      $ (Text.drop . fromIntegral $ r._offset.runTInt) t

data RawInlineQueryVideoResult = RawInlineQueryVideoResult {
    _type :: Text
  , _id :: Text
  , _video_file_id :: T.FileId
  , _title :: Text
  , _description :: Maybe Text
  , _caption :: Maybe Text
  , _parse_mode :: Maybe T.ParseMode
  , _show_caption_above_media :: Maybe Bool
  }

$(deriveToJSON defaultOptions{fieldLabelModifier = drop 1 , omitNothingFields = True } ''RawInlineQueryVideoResult)

data RawInlineQueryPhotoResult = RawInlineQueryPhotoResult {
    _type :: Text
  , _id :: Text
  , _photo_file_id :: T.FileId
  }

$(deriveJSON defaultOptions{fieldLabelModifier = drop 1 } ''RawInlineQueryPhotoResult)

instance ToJSON T.InlineQueryResult where
  toJSON (T.InlineQueryResultCachedVideo i v t d c pm scam) =
    toJSON do RawInlineQueryVideoResult "video" i v t d c pm scam
  toJSON (T.InlineQueryResultCachedPhoto i v) =
    toJSON do RawInlineQueryPhotoResult "photo" i v

$(deriveToJSON defaultOptions{ omitNothingFields = True } ''T.InlineQueryAnswer)
$(deriveToJSON defaultOptions{ omitNothingFields = True } ''T.CallbackQueryAnswer)
$(deriveToJSON defaultOptions{ omitNothingFields = True } ''T.EditMessageReplyMarkup)

data File = File {
    file_id :: T.FileId
  -- , file_unique_id :: UFileId
  , file_size :: Maybe Int
  , file_path :: Maybe Text
  } deriving (Generic,Eq,Ord,Show,FromJSON)

instance ToJSON T.CommandScope where
  toJSON T.CommandScopeDefault = object [ "type" .= ("default" :: Text) ]
  toJSON T.CommandScopeAllPrivateChats = object [ "type" .= ("all_private_chats" :: Text) ]
  toJSON T.CommandScopeAllGroupChats = object [ "type" .= ("all_group_chats" :: Text) ]
  toJSON T.CommandScopeAllChatAdministrators = object [ "type" .= ("all_chat_administrators" :: Text) ]
  toJSON (T.CommandScopeChat cid) = object [
      "type" .= ("chat" :: Text)
    , "chat_id" .= cid
    ]
  toJSON (T.CommandScopeChatAdministrators cid) = object [
      "type" .= ("chat_administrators" :: Text)
    , "chat_id" .= cid
    ]
  toJSON (T.CommandScopeChatMember cid uid) = object [
      "type" .= ("chat_member" :: Text)
    , "chat_id" .= cid
    , "user_id" .= uid
    ]

