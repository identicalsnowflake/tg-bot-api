module Telegram.Monad where

import Control.Lens
import Control.Monad.Except
import Data.ByteString (ByteString)
import Network.HTTP.Simple (Request)

import Telegram.Methods
import Telegram.Types (UserId)


newtype HTTPRequest b = HttpRequest (Traversal () (ResponseStatus b) Request (ResponseStatus ByteString))

class Monad m => MonadHTTP m where
  sendRequests :: HTTPRequest b -> m (ResponseStatus b)

class Monad m => MonadTelegram m where
  myUserId :: m UserId
  viaTelegram :: TRequest b -> m (HTTPRequest b)

class (MonadError HTTPFailure m , MonadHTTP m , MonadTelegram m) => MonadSTelegram m where
  throwTelegram :: TRequest b -> m b

