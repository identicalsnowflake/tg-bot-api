{-# LANGUAGE RecordWildCards #-}
{-# LANGUAGE StrictData #-}

module Telegram.Methods
       ( TRequest
       , OutgoingMessage(..)
       , ChatMember(..)
       , MemberStatus(..)
       , isCurrentMember
       , isAdmin
       , ResponseStatus(..)
       , AdminPredicates(..)
       , RestrictedPredicates(..)
       , HTTPFailure(..)
       , VideoMeta(..)
       , setWebhook
       , getChatAdministrators
       , getChannelMember
       , getGroupMember
       , getMe
       , getUserProfilePhotos
       , getFile
       , downloadFile
       , unbanChannelMember
       , sendMessage
       , sendDocument
       , sendVideoFile
       , sendVideoFileConduit
       , sendExistingVideo
       , sendAudioFile
       , sendImage
       , deleteMessage
       , pinChatMessage
       , unpinChatMessage
       , CreateInviteLink(..)
       , createChatInviteLink
       , exportChatInviteLink
       , revokeChatInviteLink
       , approveChatJoinRequest
       , declineChatJoinRequest
       , answerCallbackQuery
       , answerInlineQuery
       , editMessageReplyMarkup
       , setMyCommands
       , getMyCommands
       , deleteMyCommands
       , setMyName
       , getMyName
       , setMyDescription
       , getMyDescription
       , setMyShortDescription
       , getMyShortDescription
       , withToken
       ) where

import qualified CMarkGFM as CM
import Conduit
import Control.Concurrent
import qualified Control.Concurrent.Chan as C
import Control.Lens hiding (Traversing)
import Control.Monad
import qualified Data.Aeson as AE
import Data.Aeson.TH
import Data.ByteString (ByteString)
import qualified Data.ByteString.Char8 as C8
import Data.ByteString.Lazy (toStrict)
import Data.Char (toLower)
import Data.Coerce
import Data.Maybe
import Data.Text (Text)
import qualified Data.Text as Text
import Data.Text.Encoding (encodeUtf8)
import GHC.Generics
import qualified Network.HTTP.Client
import qualified Network.HTTP.Conduit as C
import Network.HTTP.Simple hiding (httpLbs)
import qualified Network.HTTP.Client.MultipartFormData as MP
import System.IO.Unsafe

import qualified Telegram.LowLevelTypes as LL
import qualified Telegram.Types as T


-- slightly clunky bc of the first @ResponseStatus@ here,
-- but should allow to easily aggregate errors or treat them granularly
-- | Applicative, allowing for batching commands to Telegram when order isn't important.
newtype TRequest a = TRequest (Traversal TelegramBotToken (ResponseStatus a) (TelegramBotToken -> Request) (ResponseStatus ByteString))

withToken :: Functor f => TRequest a -> TelegramBotToken -> ([ Request ] -> f [ ResponseStatus ByteString ]) -> f (ResponseStatus a)
withToken (TRequest f) t r = (unsafePartsOf f \xs -> r ((\g -> g t) <$> xs)) t

instance Functor TRequest where
  {-# INLINE fmap #-}
  fmap f = \(TRequest g) -> TRequest \x -> fmap (fmap f) . g x

instance Applicative TRequest where
  {-# INLINE pure #-}
  pure x = TRequest \_ _ -> pure (pure x)
  {-# INLINE (<*>) #-}
  (<*>) (TRequest f) (TRequest x) = TRequest \g a -> do
    liftA2 ($) <$> f g a <*> x g a


type TelegramBotToken = String

data HTTPFailure =
    Rejected Int ByteString
  | Failed C.HttpException
  deriving (Generic,Show)

data ResponseStatus a =
    Success a
  | Failure HTTPFailure
  deriving (Generic,Show,Functor,Foldable,Traversable)

instance Applicative ResponseStatus where
  pure = Success
  (<*>) = ap

instance Monad ResponseStatus where
  (>>=) (Success x) f = f x
  (>>=) (Failure x) _ = Failure x


data MemberStatus' =
    Creator'
  | Administrator'
  | Member'
  | Restricted'
  | Left'
  | Kicked'
  deriving (Generic,Eq,Ord,Show)

$(deriveJSON defaultOptions{ constructorTagModifier = fmap toLower . init } ''MemberStatus')

data OutgoingMessage' = OutgoingMessage' {
    chat_id :: T.ChatId -- apparently this can also be a channel name?
  , text :: Text
  , parse_mode :: T.ParseMode
  , disable_notification :: Bool -- sends notification with no sound
  , reply_to_message_id :: Maybe T.MessageId
  , reply_markup :: Maybe T.InlineKeyboardMarkup
  } deriving Generic

$(deriveToJSON defaultOptions{ omitNothingFields = True } ''OutgoingMessage')

data OutgoingMessage = OutgoingMessage {
    chat_id :: T.ChatId -- apparently this can also be a channel name?
  , text :: Text
  , parse_mode :: T.ParseMode
  , disable_notification :: Bool -- sends notification with no sound
  , reply_to_message_id :: Maybe T.MessageId
  , reply_markup :: Maybe T.InlineKeyboardMarkup
  } deriving Generic

instance AE.ToJSON OutgoingMessage where
  toJSON = \x -> case x.parse_mode of
    T.GFM -> AE.toJSON case x of
      OutgoingMessage { .. } -> OutgoingMessage' { text = gfmToMarkdownV2 text , parse_mode = T.MarkdownV2 , .. }      
    _ -> AE.toJSON case x of
      OutgoingMessage { .. } -> OutgoingMessage' { .. }

    where
      -- | Much less brittle way to send Markdown, which should elide almost all of the busted parse errors Telegram is wont to give.
      gfmToMarkdownV2 :: Text -> Text
      gfmToMarkdownV2 = encodeNode . CM.commonmarkToNode [] []
        where
          encodeNode :: CM.Node -> Text
          encodeNode (CM.Node _ t cs) = do
            let ct = Text.concat $ encodeNode <$> cs
            case t of
              CM.TEXT x -> cleanRender x <> ct
              CM.CODE_BLOCK i x -> "```" <> i <> "\n" <> x <> "```" <> ct
              CM.LINEBREAK -> "\n" <> ct
              CM.EMPH -> "*" <> ct <> "*"
              CM.STRONG -> "_" <> ct <> "_"
              CM.LINK u _ -> "[" <> ct <> "](" <> u <> ")"
              CM.STRIKETHROUGH -> "~" <> ct <> "~"
              CM.BLOCK_QUOTE -> "> " <> ct
              CM.CODE x -> "`" <> cleanRender x <> "`" <> ct
              _ -> ct

          escapeChar :: Char -> Bool
          escapeChar c = c `elem` ['_', '*', '[', ']', '(', ')', '~', '`', '>', '#', '+', '-', '=', '|', '{', '}', '.', '!','\\']

          cleanRender :: Text -> Text
          cleanRender t = flip foldMap (Text.unpack t) \c -> if escapeChar c then Text.pack [ '\\' , c ] else Text.singleton c


sendMessage :: OutgoingMessage -> TRequest T.Message
sendMessage = makeRequest' LL.parseRawMessage "sendMessage"

editMessageReplyMarkup :: T.ChatId -> T.MessageId -> Maybe T.InlineKeyboardMarkup -> TRequest T.Message
editMessageReplyMarkup cid mid mikm = makeRequest' LL.parseRawMessage "editMessageReplyMarkup"
  $ T.EditMessageReplyMarkup cid mid mikm

data OutgoingExistingVideo = OutgoingExistingVideo {
    chat_id :: T.ChatId
  , video :: T.FileId
  } deriving (Generic,Eq,Ord,Show,AE.ToJSON)

sendExistingVideo :: (T.ChatId , T.FileId) -> TRequest T.Message
sendExistingVideo = lmap (uncurry OutgoingExistingVideo) do
  makeRequest' LL.parseRawMessage "sendVideo"

{-# NOINLINE sendDocument #-}
sendDocument :: T.ChatId -> FilePath -> ByteString -> Maybe T.MessageId -> TRequest T.Message
sendDocument cid fp docbod mreply = TRequest \f _ -> fmap (dec LL.parseRawMessage) <$> f mpreq
  where
    mpreq :: TelegramBotToken -> Request
    mpreq t = unsafePerformIO do
      -- really annoying API
      setRequestMethod "POST" <$> do
        MP.formDataBody
          do [    MP.partBS "chat_id" (C8.pack (show cid))
                , MP.partFileRequestBody "document" fp (C.RequestBodyBS docbod)
                ] <> case mreply of
                       Nothing -> []
                       Just rid -> [ MP.partBS "reply_to_message_id" (C8.pack (show rid)) ]
          do parseRequest_ $ "https://api.telegram.org/bot" <> t <> "/sendDocument"
    
    dec :: AE.FromJSON a => (a -> Maybe b) -> ByteString -> b
    dec f x = case AE.eitherDecodeStrict' x of
      Left e -> error $ "Unexpected JSON format received from Telegram: " <> e
      Right (TWrapper y) -> case f y of
        Nothing -> error $ "Could not reify low-level API type into high-level type."
        Just z -> z

data VideoMeta = VideoMeta {
    duration_s :: Maybe Int
  , width :: Maybe Int
  , height :: Maybe Int
  , supports_streaming :: Maybe Bool
  
  , caption :: Maybe Text
  , show_caption_above_media :: Maybe Bool
  , spoiler :: Maybe Bool
  , protect_content :: Maybe Bool
  }

{-# NOINLINE sendVideoFile #-}
sendVideoFile :: T.ChatId -> FilePath -> VideoMeta -> TRequest T.Message
sendVideoFile cid fp vm = TRequest \f _ -> fmap (dec LL.parseRawMessage) <$> f mpreq
  where
    mpreq :: TelegramBotToken -> Request
    mpreq t = unsafePerformIO do
      -- really annoying API
      setRequestMethod "POST" <$> do
        MP.formDataBody do
            catMaybes [
                vm.duration_s <&> \x -> MP.partBS "duration" (C8.pack (show x))
              , vm.width <&> \x -> MP.partBS "width" (C8.pack (show x))
              , vm.height <&> \x -> MP.partBS "height" (C8.pack (show x))
              , vm.supports_streaming <&> \x -> MP.partBS "supports_streaming" (lbool x)
              , vm.caption <&> \x -> MP.partBS "caption" (encodeUtf8 x)
              , vm.show_caption_above_media <&> \x -> MP.partBS "show_caption_above_media" (lbool x)
              , vm.spoiler <&> \x -> MP.partBS "has_spoiler" (lbool x)
              , vm.protect_content <&> \x -> MP.partBS "protect_content" (lbool x)
              , Just $ MP.partBS "chat_id" (C8.pack (show cid))
              , Just $ MP.partFileSource "video" fp
              ]
          $ parseRequest_ $ "https://api.telegram.org/bot" <> t <> "/sendVideo"
    
    dec :: AE.FromJSON a => (a -> Maybe b) -> ByteString -> b
    dec f x = case AE.eitherDecodeStrict' x of
      Left e -> error $ "Unexpected JSON format received from Telegram: " <> e
      Right (TWrapper y) -> case f y of
        Nothing -> error $ "Could not reify low-level API type into high-level type."
        Just z -> z

-- allows for streaming decoding/uploading
sendVideoFileConduit :: T.ChatId -> ConduitT () ByteString (ResourceT IO) () -> VideoMeta -> TRequest T.Message
sendVideoFileConduit cid source vm = TRequest \f _ -> fmap (dec LL.parseRawMessage) <$> f mpreq
  where
    bpop :: Network.HTTP.Client.GivesPopper ()
    bpop f = do
      c <- C.newChan @(Maybe ByteString)
      forkIO do
        !() <- runConduitRes $ source .| mapM_C (liftIO . C.writeChan c . Just)
        C.writeChan c Nothing
      f do
        C.readChan c >>= \case
            Nothing -> pure ""
            Just bs -> pure bs
    
    mpreq :: TelegramBotToken -> Request
    mpreq t = unsafePerformIO do
      req <- parseRequest $ "https://api.telegram.org/bot" <> t <> "/sendVideo"
      req' <- setRequestMethod "POST"
               <$> flip MP.formDataBody req do
                     catMaybes [
                         vm.duration_s <&> \x -> MP.partBS "duration" (C8.pack (show x))
                       , vm.width <&> \x -> MP.partBS "width" (C8.pack (show x))
                       , vm.height <&> \x -> MP.partBS "height" (C8.pack (show x))
                       , vm.supports_streaming <&> \x -> MP.partBS "supports_streaming" (lbool x)
                       , vm.caption <&> \x -> MP.partBS "caption" (encodeUtf8 x)
                       , vm.show_caption_above_media <&> \x -> MP.partBS "show_caption_above_media" (lbool x)
                       , vm.spoiler <&> \x -> MP.partBS "has_spoiler" (lbool x)
                       , vm.protect_content <&> \x -> MP.partBS "protect_content" (lbool x)
                       , Just $ MP.partBS "chat_id" (C8.pack (show cid))
                       , Just $ MP.partFileRequestBody "video" "video.mp4"
                                  $ C.RequestBodyStreamChunked bpop
                       ]
      pure req'
    
    dec :: AE.FromJSON a => (a -> Maybe b) -> ByteString -> b
    dec f x = case AE.eitherDecodeStrict' x of
      Left e -> error $ "Unexpected JSON format received from Telegram: " <> e
      Right (TWrapper y) -> case f y of
        Nothing -> error $ "Could not reify low-level API type into high-level type."
        Just z -> z

{-# NOINLINE sendAudioFile #-}
sendAudioFile :: T.ChatId -> FilePath -> Maybe Text -> TRequest T.Message
sendAudioFile cid fp mtitle = TRequest \f _ -> fmap (dec LL.parseRawMessage) <$> f mpreq
  where
    mpreq :: TelegramBotToken -> Request
    mpreq t = unsafePerformIO do
      -- really annoying API
      setRequestMethod "POST" <$> do
        MP.formDataBody
          do [ MP.partBS "chat_id" (C8.pack (show cid))
              , MP.partFileSource "audio" fp
              ] <> case mtitle of
                    Nothing -> []
                    Just title -> [ MP.partBS "chat_id" (encodeUtf8 (Text.take 1000 title)) ]
          do parseRequest_ $ "https://api.telegram.org/bot" <> t <> "/sendAudio"
    
    dec :: AE.FromJSON a => (a -> Maybe b) -> ByteString -> b
    dec f x = case AE.eitherDecodeStrict' x of
      Left e -> error $ "Unexpected JSON format received from Telegram: " <> e
      Right (TWrapper y) -> case f y of
        Nothing -> error $ "Could not reify low-level API type into high-level type."
        Just z -> z

{-# NOINLINE sendImage #-}
sendImage :: T.ChatId -> ByteString -> Maybe Text -> TRequest T.Message
sendImage cid im_bytes mcaptions = TRequest \f _ -> fmap (dec LL.parseRawMessage) <$> f mpreq
  where
    mpreq :: TelegramBotToken -> Request
    mpreq t = unsafePerformIO do
      -- really annoying API
      setRequestMethod "POST" <$> do
        MP.formDataBody do
              maybe [] (\c -> pure $ MP.partBS "caption" $ encodeUtf8 c) mcaptions
                <> [ MP.partBS "chat_id" (C8.pack (show cid))
                   , MP.partFileRequestBody "photo" "image.jpg"
                         $ C.RequestBodyBS im_bytes
                   ]
          $ parseRequest_ $ "https://api.telegram.org/bot" <> t <> "/sendPhoto"
    
    dec :: AE.FromJSON a => (a -> Maybe b) -> ByteString -> b
    dec f x = case AE.eitherDecodeStrict' x of
      Left e -> error $ "Unexpected JSON format received from Telegram: " <> e
      Right (TWrapper y) -> case f y of
        Nothing -> error $ "Could not reify low-level API type into high-level type."
        Just z -> z

answerInlineQuery :: T.InlineQueryAnswer -> TRequest ()
answerInlineQuery = makeRequest' Just "answerInlineQuery"

answerCallbackQuery :: T.CallbackQueryAnswer -> TRequest ()
answerCallbackQuery = makeRequest' Just "answerCallbackQuery"

data ChatMember' = ChatMember' {
    user :: T.User
  , status :: MemberStatus'
  , until_date :: Maybe T.Timestamp
    -- admin attributes
  , can_be_edited :: Maybe Bool
  , can_change_info :: Maybe Bool
  , can_post_messages :: Maybe Bool
  , can_edit_messages :: Maybe Bool
  , can_delete_messages :: Maybe Bool
  , can_invite_users :: Maybe Bool
  , can_restrict_members :: Maybe Bool
  , can_pin_messages :: Maybe Bool
  , can_promote_members :: Maybe Bool
    -- restricted attributes
  , is_member :: Maybe Bool
  , can_send_messages :: Maybe Bool
  , can_send_media_messages :: Maybe Bool
  , can_send_other_messages :: Maybe Bool
  , can_add_web_page_previews :: Maybe Bool
  } deriving (Generic,Eq,Ord,Show,AE.FromJSON)

data AdminPredicates = AdminPredicates {
    can_be_edited :: Bool
  , can_change_info :: Bool
  , can_post_messages :: Bool
  , can_edit_messages :: Bool
  , can_delete_messages :: Bool
  , can_invite_users :: Bool
  , can_restrict_members :: Bool
  , can_pin_messages :: Bool
  , can_promote_members :: Bool
  } deriving (Generic,Eq,Ord,Show)

data RestrictedPredicates = RestrictedPredicates {
    is_member :: Bool
  , can_send_messages :: Bool
  , can_send_media_messages :: Bool
  , can_send_other_messages :: Bool
  , can_add_web_page_previews :: Bool
  } deriving (Generic,Eq,Ord,Show)

data MemberStatus =
    Administrator AdminPredicates
  | Creator
  | Member
  | Former
  | Kicked
  | Restricted RestrictedPredicates
  deriving (Generic,Eq,Ord,Show)

isCurrentMember :: MemberStatus -> Bool
isCurrentMember (Administrator _) = True
isCurrentMember Creator = True
isCurrentMember Member = True
isCurrentMember (Restricted _) = True
isCurrentMember Former = False
isCurrentMember Kicked = False

isAdmin :: MemberStatus -> Bool
isAdmin (Administrator _) = True
isAdmin Creator = True
isAdmin _ = False

data ChatMember = ChatMember {
    user :: T.User
  , status :: MemberStatus
  } deriving (Generic,Eq,Ord,Show)

instance AE.FromJSON ChatMember where
  parseJSON = fmap memberStatus . AE.parseJSON

memberStatus :: ChatMember' -> ChatMember
memberStatus ChatMember' { .. } = ChatMember user $ case status of
  Creator' -> Creator
  Member' -> Member
  Left' -> Former
  Kicked' -> Kicked
  Administrator' -> Administrator $ AdminPredicates {
      can_be_edited = maybe True id can_be_edited
    , can_change_info = maybe True id can_change_info
    , can_post_messages = maybe True id can_post_messages
    , can_edit_messages = maybe True id can_edit_messages
    , can_delete_messages = maybe True id can_delete_messages
    , can_invite_users = maybe True id can_invite_users
    , can_restrict_members = maybe True id can_restrict_members
    , can_pin_messages = maybe True id can_pin_messages
    , can_promote_members = maybe True id can_promote_members
    }
  Restricted' -> Restricted $ RestrictedPredicates {
      is_member = maybe True id is_member
    , can_send_messages = maybe True id can_send_messages
    , can_send_media_messages = maybe True id can_send_media_messages
    , can_send_other_messages = maybe True id can_send_other_messages
    , can_add_web_page_previews = maybe True id can_add_web_page_previews
    }
  
setWebhook :: Text -> Text -> TRequest ()
setWebhook url secretToken =
  makeRequest (\u -> Just u) (\_ -> ("setWebhook" , [] , Just b)) (url,secretToken)
  where
    b = toStrict $ AE.encode $ AE.object [
        "url" AE..= url
      , "secret_token" AE..= secretToken
      ]

data UserProfilePhotos = UserProfilePhotos {
    total_count :: Int
  , photos :: [ [ T.PhotoSize ] ]
  } deriving (Generic,AE.FromJSON)

getUserProfilePhotos :: T.UserId -> TRequest [ [ T.PhotoSize ] ]
getUserProfilePhotos uid = (\(x :: UserProfilePhotos) -> x.photos) <$> 
  makeRequest_ ("getUserProfilePhotos" , [ ("user_id" , toStrict $ AE.encode uid) ] , Nothing)

getFile :: T.FileId -> TRequest T.File
getFile fid =  TRequest $ \f tn -> fmap (dec tn) <$> f do
  makeR ("getFile" , [ ("file_id" , encodeUtf8 $ (coerce fid :: Text) ) ] , Nothing)
  where    
    dec :: TelegramBotToken -> ByteString -> T.File
    dec t x = case AE.eitherDecodeStrict' x of
      Left e -> error $ "Unexpected JSON format received from Telegram: " <> e
      Right (TWrapper LL.File { .. }) -> T.File {
          url = file_path <&> \p -> "https://api.telegram.org/file/bot" <> Text.pack t <> "/" <> p
        , ..
        }

-- would be proper to make this lazy somehow, but it kinda messes with everything else, so w/e
downloadFile :: T.File -> TRequest ByteString
downloadFile x = TRequest \f _ -> f \_ -> parseRequest_ $ maybe "NO_PATH_GIVEN" Text.unpack x.url

getChatAdministrators :: T.ChatId -> TRequest [ ChatMember ]
getChatAdministrators =
  makeRequest pure \i -> ("getChatAdministrators" , [ ("chat_id" , toStrict $ AE.encode i) ] , Nothing)

getGroupMember :: T.ChatId -> T.UserId -> TRequest ChatMember
getGroupMember cid uid =
  makeRequest_ ("getChatMember" , [ ("chat_id" , toStrict $ AE.encode cid) , ("user_id" , toStrict $ AE.encode uid) ] , Nothing)

getChannelMember :: T.ChatId -> T.UserId -> TRequest ChatMember
getChannelMember cid uid =
  makeRequest_ ("getChatMember" , [ ("chat_id" , "-" <> do toStrict $ AE.encode cid) , ("user_id" , toStrict $ AE.encode uid) ] , Nothing)

-- note that this will *kick* the member if they're in the channel! to do nothing in this case, pass (Just True)
unbanChannelMember :: T.ChatId -> T.UserId -> Maybe Bool -> TRequest ()
unbanChannelMember cid uid only_if_banned =
  makeRequest_ ("unbanChatMember" , maybe [] (\b -> pure $ ("only_if_banned" , if b then "true" else "false")) only_if_banned <> [ ("chat_id" , "-" <> do toStrict $ AE.encode cid) , ("user_id" , toStrict $ AE.encode uid) ] , Nothing)

pinChatMessage :: T.ChatId -> T.MessageId -> Bool -> TRequest ()
pinChatMessage cid mid disable_notification =
  makeRequest_ ("pinChatMessage" , [ ("chat_id" , toStrict $ AE.encode cid) , ("message_id" , toStrict $ AE.encode mid) , ("disable_notification" , if disable_notification then "true" else "false") ] , Nothing)

unpinChatMessage :: T.ChatId -> T.MessageId -> TRequest ()
unpinChatMessage cid mid =
  makeRequest_ ("unpinChatMessage" , [ ("chat_id" , toStrict $ AE.encode cid) , ("message_id" , toStrict $ AE.encode mid) ] , Nothing)

exportChatInviteLink :: T.ChatId -> TRequest Text
exportChatInviteLink cid = makeRequest pure (\_ -> ("exportChatInviteLink" , [ ("chat_id" , toStrict $ AE.encode cid)] , mempty)) ()

revokeChatInviteLink :: T.ChatId -> Text -> TRequest ()
revokeChatInviteLink cid link = makeRequest pure (\_ -> ("revokeChatInviteLink" , [ ("chat_id" , toStrict $ AE.encode cid) , ("invite_link" , encodeUtf8 link)] , mempty)) ()

data CreateInviteLink = CreateInviteLink {
    chat_id :: T.ChatId
  , name :: Maybe Text
  , expire_date :: Maybe Int -- unix timestamp
  , member_limit :: Maybe Int
  , creates_join_request :: Maybe Bool
  }

createChatInviteLink :: CreateInviteLink -> TRequest T.ChatInviteLink
createChatInviteLink x = do
  makeRequest_ ("createChatInviteLink" , [] , Just $ toStrict $ AE.encode b)
  where
    b = object' [ "chat_id" AE..= x.chat_id
                , "name" AE..= x.name
                , "expire_date" AE..= fmap (T.TInt . fromIntegral) x.expire_date
                , "member_limit" AE..= fmap (T.TInt . fromIntegral) x.member_limit
                , "creates_join_request" AE..= x.creates_join_request
                ]


approveChatJoinRequest :: T.ChatId -> T.UserId -> TRequest ()
approveChatJoinRequest cid uid =
  makeRequest_ ("approveChatJoinRequest" , [ ("chat_id" , toStrict $ AE.encode cid) , ("user_id" , toStrict $ AE.encode uid) ] , Nothing)

declineChatJoinRequest :: T.ChatId -> T.UserId -> TRequest ()
declineChatJoinRequest cid uid =
  makeRequest_ ("declineChatJoinRequest" , [ ("chat_id" , toStrict $ AE.encode cid) , ("user_id" , toStrict $ AE.encode uid) ] , Nothing)


getMe :: TRequest T.User
getMe = makeRequest pure (\_ -> ("getMe" , mempty , mempty)) ()

setMyCommands :: [ T.Command ] -> Maybe T.CommandScope -> Maybe T.LanguageCode -> TRequest ()
setMyCommands cs mcs ml = do
  makeRequest_ ("setMyCommands" , [] , Just $ toStrict $ AE.encode b)
  where
    b = object' [ "commands" AE..= cs
                , "scope" AE..= mcs
                , "language_code" AE..= ml
                ]

deleteMyCommands :: Maybe T.CommandScope -> Maybe T.LanguageCode -> TRequest ()
deleteMyCommands mcs ml = do
  makeRequest_ ("deleteMyCommands" , [] , Just $ toStrict $ AE.encode b)
  where
    b = object' [ "scope" AE..= mcs
                , "language_code" AE..= ml
                ]

getMyCommands :: Maybe T.CommandScope -> Maybe T.LanguageCode -> TRequest [ T.Command ]
getMyCommands mcs ml = do
  makeRequest_ ("getMyCommands" , [] , Just $ toStrict $ AE.encode b)
  where
    b = object' [ "scope" AE..= mcs
                , "language_code" AE..= ml
                ]

setMyName :: Maybe Text -> Maybe T.LanguageCode -> TRequest ()
setMyName mn ml = do
  makeRequest_ ("setMyName" , [] , Just $ toStrict $ AE.encode b)
  where
    b = object' [ "name" AE..= mn
                , "language_code" AE..= ml
                ]

data BotName = BotName {
    name :: Text
  } deriving (Generic,AE.FromJSON)

getMyName :: Maybe Text -> Maybe T.LanguageCode -> TRequest Text
getMyName mn ml = do
  makeRequest_ ("getMyName" , [] , Just $ toStrict $ AE.encode b) <&> \(BotName n) -> n
  where
    b = object' [ "name" AE..= mn
                , "language_code" AE..= ml
                ]

setMyDescription :: Maybe Text -> Maybe T.LanguageCode -> TRequest ()
setMyDescription mn ml = do
  makeRequest_ ("setMyDescription" , [] , Just $ toStrict $ AE.encode b)
  where
    b = object' [ "description" AE..= mn
                , "language_code" AE..= ml
                ]

data BotDescription = BotDescription {
    description :: Text
  } deriving (Generic,AE.FromJSON)

getMyDescription :: Maybe Text -> Maybe T.LanguageCode -> TRequest Text
getMyDescription mn ml = do
  makeRequest_ ("getMyDescription" , [] , Just $ toStrict $ AE.encode b) <&> \(BotDescription n) -> n
  where
    b = object' [ "description" AE..= mn
                , "language_code" AE..= ml
                ]


setMyShortDescription :: Maybe Text -> Maybe T.LanguageCode -> TRequest ()
setMyShortDescription mn ml = do
  makeRequest_ ("setMyShortDescription" , [] , Just $ toStrict $ AE.encode b)
  where
    b = object' [ "short_description" AE..= mn
                , "language_code" AE..= ml
                ]

data BotShortDescription = BotShortDescription {
    short_description :: Text
  } deriving (Generic,AE.FromJSON)

getMyShortDescription :: Maybe Text -> Maybe T.LanguageCode -> TRequest Text
getMyShortDescription mn ml = do
  makeRequest_ ("getMyShortDescription" , [] , Just $ toStrict $ AE.encode b) <&> \(BotShortDescription n) -> n
  where
    b = object' [ "short_description" AE..= mn
                , "language_code" AE..= ml
                ]



deleteMessage :: T.ChatId -> T.MessageId -> TRequest ()
deleteMessage cid mid = makeRequest_ ("deleteMessage" , [ ("chat_id" , toStrict $ AE.encode cid) , ("message_id" , toStrict $ AE.encode mid) ] , Nothing)

makeRequest' :: (AE.ToJSON a , AE.FromJSON b)
             => (b -> Maybe c) -> String -> a -> TRequest c
makeRequest' h u = makeRequest h \x -> (u , [] , Just $ toStrict $ AE.encode x)

makeRequest_ :: AE.FromJSON a
             => (String , [ (ByteString , ByteString) ] , Maybe ByteString) -> TRequest a
makeRequest_ r = makeRequest pure (\() -> r) ()

data TWrapper a = TWrapper {
    result :: a
  } deriving (Generic,AE.FromJSON)

makeRequest :: AE.FromJSON b
            => (b -> Maybe c) -> (a -> (String , [ (ByteString , ByteString) ] , Maybe ByteString)) -> a -> TRequest c
makeRequest h g = \x -> TRequest $ \f _ -> fmap (dec h) <$> f (makeR (g x))
  where
    dec :: AE.FromJSON a => (a -> Maybe b) -> ByteString -> b
    dec f x = case AE.eitherDecodeStrict' x of
      Left e -> error $ "Unexpected JSON format received from Telegram: " <> e
      Right (TWrapper y) -> case f y of
        Nothing -> error $ "Could not reify low-level API type into high-level type."
        Just z -> z

makeR :: (String , [(ByteString , ByteString)] , Maybe ByteString) -> TelegramBotToken -> Request
makeR (u , qs , Just b) t =
    setRequestMethod "POST"
  . setRequestBody (C.RequestBodyBS b)
  . setRequestHeaders [("Content-Type","application/json")]
  . setRequestQueryString (over (mapped . _2) pure qs)
  $ parseRequest_ $ "https://api.telegram.org/bot" <> t <> "/" <> u
makeR (u , qs , Nothing) t =
    setRequestMethod "GET"
  . setRequestQueryString (over (mapped . _2) pure qs)
  $ parseRequest_ $ "https://api.telegram.org/bot" <> t <> "/" <> u

lbool :: Bool -> ByteString
lbool True = "true"
lbool False = "false"

-- remove nulls at the top level, since Telegram does distinguish between a value being null and it being elided, and the latter is always what's intended
object' :: [ (AE.Key , AE.Value) ] -> AE.Value
object' xs = AE.object $ filter ((AE.Null/=) . snd) xs

