{-# LANGUAGE RecordWildCards #-}
{-# LANGUAGE DuplicateRecordFields #-}

module App.Utils where

import Control.Lens hiding (from)

import Telegram.Types


data ReplyContext = ReplyContext {
    chat_id :: ChatId
  , chat_type :: ChatType
  , from :: Maybe User
  , message_id :: MessageId
  , message :: Message
  }

newMessage :: Traversal' Incoming ReplyContext
newMessage f o@New { .. } = case notification of
  DM n -> case n of
    IncomingMessage m -> const o <$> f ReplyContext { chat_type = Direct , message = m , .. }
    _ -> pure o
  GroupNotification t gn -> case gn of
    General n -> case n of
      IncomingMessage m -> const o <$> f ReplyContext { chat_type = NonDirect t , message = m , .. }
      _ -> pure o
    _ -> pure o
newMessage _ x = pure x

